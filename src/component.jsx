import React from 'react';
import PropTypes from 'prop-types';
import { LionLogoIcon } from '@nypl/dgx-svg-icons';

const HomepageStaffPicks = ({
  id,
  className,
  items,
  itemsToDisplay,
  errorMessage,
  error,
  lang,
  gaClickEvent,
}) => {
  /**
   * getLanguageText(obj)
   * @desc it generates the object especially for title and description and adds fallbacks
   * @param {obj} obj is the object of title or description
   * @return {string} it returns text as a string
   */
  const getLanguageText = (obj) => (
    (obj && obj[lang] && obj[lang].text) ? obj[lang].text : ''
  );

  /**
   * generateItemsToDisplay(items, itemsToDisplay, bemName)
   * @desc it generates the contents we need for each item and adds fallbacks
   * @param {items} items is the data includes every item
   * @param {itemsToDisplay} determines how many items to show on the page
   * @param {bemName} passes the class name
   * @return {object} it returns the DOM of the individual item
   */
  const generateItemsToDisplay = (itemsArray, itemsToDisplayCount, bemName) => (
    itemsArray.slice(0, itemsToDisplayCount).map((element, i) => {
      const authorImgFallback =
        <LionLogoIcon ariaHidden fill="transparent" height={25} width={25} />;
      const {
        description: description = {},
        link = '',
        image: image = {},
        author: author = {},
      } = element;

      const descriptionText = getLanguageText(description);
      const imageContent = (image && image.bookCoverImage) ? image.bookCoverImage : {};
      const {
        'full-uri': fullUri = '',
        alt: bookDescription = 'We\'re sorry. The image isn\'t available for this item.',
      } = imageContent;
      const {
        firstName: firstName = '',
        lastName: lastName = '',
        title: authorTitle = '',
      } = author;
      const gaActionText = `Staff Picks - ${i + 1}`;

      return (
        <div className={`${bemName}-wrapper`} key={i}>
          <div className={`${bemName}-pick`}>
            <div className="quoteBackground"></div>
            <p className="description">
              {descriptionText}
            </p>
            <a
              className="link"
              href={link}
              onClick={gaClickEvent ? () => gaClickEvent(gaActionText, link) : null}
            >
              <img src={fullUri} alt={bookDescription} className="bookCoverImage" />
            </a>
          </div>
          <div className={`${bemName}-staff`}>
            {authorImgFallback}
            <p className="name">{firstName} {lastName}</p>
            <p className="title">{authorTitle}</p>
          </div>
        </div>
      );
    })
  );

  /**
   * renderFailure(errorMsg)
   * @desc returns a failure DOM container with an error
   * message established via prop
   * @param {errorMsg} string representation of an error message
   * @return {object} it returns the error message of the component
   */
  const renderFailure = (errorMsg) => (
    <div className="errorMessage">
      {errorMsg}
    </div>
  );

  /**
   * renderSuccess(data)
   * @desc returns a success DOM container where the
   * component data is passed to generateItemsToDisplay()
   * for proper iteration and rendering.
   * @param {array} data is the main conent includes every item of the component
   * @return {object} it returns the DOM of the component
   */
  const renderSuccess = (data) => (
    <div className={className} id={id}>
      {generateItemsToDisplay(data, itemsToDisplay, className)}
    </div>
  );

  return (error || !items || !items.length) ?
    renderFailure(errorMessage) : renderSuccess(items);
};

HomepageStaffPicks.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  lang: PropTypes.string,
  itemsToDisplay: PropTypes.number,
  items: PropTypes.array,
  error: PropTypes.string,
  errorMessage: PropTypes.string,
  gaClickEvent: PropTypes.func,
};

HomepageStaffPicks.defaultProps = {
  id: 'hpStaffPicks',
  className: 'hpStaffPicks',
  lang: 'en',
  itemsToDisplay: 2,
  items: [],
  error: '',
  errorMessage: 'We\'re sorry. Information isn\'t available for this feature.',
};

export default HomepageStaffPicks;
