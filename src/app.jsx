import React from 'react';
import { render } from 'react-dom';
import HomepageStaffPicks from './component';

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

const dummyContent = [
  {
    title: {
      en: {
        text: 'Satin Island',
      },
    },
    description: {
      en: {
        text: 'An experimental, poetic novel obsessed with the flow of data and ' +
          'the meaning of information...',
      },
    },
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/satin_island.jpg',
        description: 'This is the description of the book',
        alt: 'This is the alt of the book',
      },
    },
    link: 'http://www.nypl.org/browse/recommendations/staff-picks/2015-10-01/' +
      '9780307593955-satin-island/',
    author: {
      firstName: 'Genoveve',
      lastName: 'Stowell',
      title: 'Managing Librarian',
    },
  },
  {
    title: {
      en: {
        text: 'Plastic: A Toxic Love Story',
      },
    },
    description: {
      en: {
        text: 'An eye-opening discussion of our love/hate relationship with plastic, ' +
          'told through the histories of eight ordinary objects...',
      },
    },
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/plastic.jpg',
        description: 'This is the description of the book',
        alt: 'This is the alt of the book',
      },
    },
    link: 'http://www.nypl.org/browse/recommendations/staff-picks/2015-11-01/' +
      '9780547152400-plastic-a-toxic-love-story/',
    author: {
      firstName: 'Maura',
      lastName: 'Muller',
      title: 'Manager, Volunteer Program',
    },
  },
];
const homepageStaffPicks = (
  <HomepageStaffPicks
    name={'HP-StaffPicks'}
    id={'HP-StaffPicks'}
    className={'RightColumn'}
    items={dummyContent}
    gaClickEvent={gaClickTest()}
  />
);

/* app.jsx
 * Used for local development of React Components
 */
render(homepageStaffPicks, document.getElementById('staff-picks-component'));
