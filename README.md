# Homepage Staff Picks Component

This component is used to display Staff Picks in the dgx-homepage app.

## Version
> v0.3.1

## Usage

> Require `dgx-homepage-staff-picks-component` as a dependency in your `package.json` file.

```sh
"dgx-homepage-staff-picks-component": "git+ssh://git@bitbucket.org/NYPL/dgx-homepage-staff-picks-component.git#master"
```

> Once installed, import the component in your React Application.

```sh
import HomepageStaffPicks from 'dgx-homepage-staff-picks-component';
```

> You may initialize the component with the following properties:

```sh
  <HomepageStaffPicks
    id="HP-StaffPicks"
    className="RightColumn"
    items={apiData}
    itemsToDisplay={4}
  />
```

> An example of an individual item object:

```sh
  {
    title: {
      en: {
        text: 'Satin Island',
      },
    },
    description: {
      en: {
        text: 'An experimental, poetic novel obsessed with the flow of data and ' +
          'the meaning of information...',
      },
    },
    image: {
      bookCoverImage: {
        'full-uri': 'https://d7.nypl.org/sites/default/files/satin_island.jpg',
        description: 'This is the description of the book',
        alt: 'This is the alt of the book',
      },
    },
    link: 'http://www.nypl.org/browse/recommendations/staff-picks/2015-10-01/' +
      '9780307593955-satin-island/',
    author: {
      firstName: 'Genoveve',
      lastName: 'Stowell',
      title: 'Managing Librarian',
    },
  }
```

## Props

- `id`: Value to use as the element id (default: "hpStaffPicks").
- `className`: Value used as the "element" in a BEM naming scheme (default: "hpStaffPicks").
- `items`: Array of items to display.
- `itemsToDisplay`: Number of staff picks to display (default: 2).
- `error`: Boolean flag to determine if an error occurred.
- `errorMessage`: String error message to display when no items are available.
- `gaClickEvent`: Function used to track Google Analytics click events for each item. The Action is determined by the string `StaffPicks - {number}` which concatenates the item number. The Label is the URL for each element (optional).
- `lang`: Language to display (default: `en`).
