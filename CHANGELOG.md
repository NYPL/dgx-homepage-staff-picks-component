## Changelog

### v0.3.1
- Updating SVG icon props for the Lion Logo.

### v0.3.1
- Updating SVG icons to @nypl/dgx-svg-icons.

### v0.2.0
- Upgrading to React 15.

### v0.1.3
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.
- Added README.md file

#### Changed
- Refactored component to state-less composition.
